//------------------------------------------------------------------------------
// This file is part of Lamprey.
//
// Lamprey is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Lamprey is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Lamprey. If not, see <https://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#pragma once

#include <string>

//------------------------------------------------------------------------------
// QueryCache
// Contains any queries which have been previously performed, allowing us to
// resume from an incomplete run.
// This class is thread-safe.
//------------------------------------------------------------------------------
class QueryCache
{
public:
    static void Populate(const std::string& filename);
    static void Add(const std::string& query);
    static bool Contains(const std::string& query);
    static size_t Size();    
};
