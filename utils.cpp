//------------------------------------------------------------------------------
// This file is part of Lamprey.
//
// Lamprey is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Lamprey is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Lamprey. If not, see <https://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#include <algorithm>
#include <fstream>

#include "utils.h"

namespace Utils
{

//------------------------------------------------------------------------------
// Trim undesired characters (\r, space) from a string.
// This cleans up search terms from ReadLines(), no matter whether the input
// file was created in Windows or Linux.
//------------------------------------------------------------------------------
void TrimRight(std::string& text)
{
	text.erase(std::find_if(text.rbegin(), text.rend(), [](int ch) 
	{
		return ch != '\r' && ch != ' ';
	}).base(), text.end());
}

//------------------------------------------------------------------------------
// Generates a vector of all non-empty lines in a file.
// Returns false if the file could not be read.
//------------------------------------------------------------------------------
bool ReadLines(const std::string& filename, std::vector<std::string>& lines)
{
	std::ifstream in(filename, std::ios::in | std::ios::binary);
  	std::string contents;
	if (in.good() == false)
	{
		return false;
	}

    in.seekg(0, std::ios::end);
    size_t contentsSize = static_cast<size_t>(in.tellg());
	contents.reserve(contentsSize);
    in.seekg(0, std::ios::beg);
    contents.assign((std::istreambuf_iterator<char>(in)), std::istreambuf_iterator<char>());
    in.close();

	size_t j = 0;
	for (size_t i = 0; i < contentsSize; ++i)
	{
		bool last = (i == contentsSize - 1);
		if (contents[i] == '\n' || last)
		{
            // Ensures the last line in the file is read correctly, whether it has a 
            // '\n' or a EOF at the end.
			size_t len = (last && contents[contentsSize - 1] != '\n') ? (i - j + 1) : (i - j);
			std::string searchTerm = contents.substr(j, len);
			TrimRight(searchTerm);
			lines.push_back(searchTerm);
			j = i + 1;			
		}
	}

    return true;
}

}
