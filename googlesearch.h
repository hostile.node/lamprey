//------------------------------------------------------------------------------
// This file is part of Lamprey.
//
// Lamprey is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Lamprey is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Lamprey. If not, see <https://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#pragma once

#include <atomic>
#include <list>
#include <mutex>
#include <string>
#include <thread>
#include <vector>

#include "query.h"
#include "json.h"

using nlohmann::json;
using CURL = void;

class GoogleSearch
{
public:
	using QueryCallback = void (*)(const QueryData& queryData, bool success, uint64_t results);

	GoogleSearch(QueryCallback pCallback);
	virtual ~GoogleSearch();

    void AsyncQuery(const std::string& query);
	bool HasFinished() const;

private:
	static void ThreadMain(GoogleSearch* pGoogleSearch);
	static std::string FilterCurlData(const std::string& data);
    static bool ExtractTotalResults(const json& data, uint64_t& result);
	static bool ExtractResults(const json& data, QueryResults& results);

	bool ConsumeQueue(QueryData& queryData);

	QueryCallback m_pQueryCallback;
	std::thread m_QueryThread;
	std::atomic_bool m_QueryThreadStopFlag;
	std::atomic_bool m_InFlight;
	CURL* m_pCurlHandle;
	std::string m_CurlData;

	using QueryQueue = std::list<QueryData>;
	QueryQueue m_QueryQueue;
	std::mutex m_QueryQueueMutex;
};
