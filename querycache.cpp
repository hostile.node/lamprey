//------------------------------------------------------------------------------
// This file is part of Lamprey.
//
// Lamprey is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Lamprey is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Lamprey. If not, see <https://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#include <iostream>
#include <fstream>
#include <mutex>
#include <string>
#include <unordered_set>
#include <vector>

#include "querycache.h"
#include "utils.h"

using QueryCacheType = std::unordered_set<std::string>;

static std::mutex s_Mutex;
static QueryCacheType s_Cache;

//------------------------------------------------------------------------------
void QueryCache::Populate(const std::string& filename)
{
    std::lock_guard<std::mutex> lock(s_Mutex);
    std::vector<std::string> lines;
    if (Utils::ReadLines(filename, lines) && !lines.empty())
    {
        for (const std::string& line : lines)
        {
            // Add the individual queries to the cache. 
            // The file format is "<result_count>,<query>", so we need to split
            // query out.
            size_t separator = line.find_first_of(',');
            if (separator != std::string::npos)
            {
                s_Cache.insert(line.substr(separator + 1));
            }
        }
    }
}
    
//------------------------------------------------------------------------------
void QueryCache::Add(const std::string& query)
{
    std::lock_guard<std::mutex> lock(s_Mutex);
    s_Cache.insert(query);
}

//------------------------------------------------------------------------------
bool QueryCache::Contains(const std::string& query)
{
    std::lock_guard<std::mutex> lock(s_Mutex);
    QueryCacheType::const_iterator it = s_Cache.find(query);
    return it != s_Cache.cend();
}

//------------------------------------------------------------------------------
size_t QueryCache::Size()
{
    std::lock_guard<std::mutex> lock(s_Mutex);
    return s_Cache.size();
}    
