//------------------------------------------------------------------------------
// This file is part of Lamprey.
//
// Lamprey is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Lamprey is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Lamprey. If not, see <https://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#include <fstream>
#include "json.h"
#include "log.h"
#include "settings.h"

// API documentation: https://developers.google.com/custom-search/v1/cse/list
// Public keys & search engine ID: 100 free searches / day.
// These are used as defaults if settings.json doesn't exist.
static std::string s_SearchEngineId("016794710981670214596:sgntarey42m");
static std::string s_ApiKey("AIzaSyBHnMEOSkTM7Lazvou27FXgJb6M4hjn9uE");

void Settings::Load()
{
    using json = nlohmann::json;
	std::ifstream file("settings.json", std::ios::in);
	if (file.is_open())
	{
		json settings;
		file >> settings;
		file.close();

        json::iterator it = settings.find("search_engine_id");
        if (it != settings.end())
        {
            s_SearchEngineId = it->get<std::string>();
        }

        it = settings.find("api_key");
        if (it != settings.end())
        {
            s_ApiKey = it->get<std::string>();
        }
	}
    else
    {
        Log::Warning("Couldn't open 'settings.json'. Using default search engine and API key.");
    }
}

std::string Settings::GetSearchEngineId()
{
    return s_SearchEngineId;
}

std::string Settings::GetApiKey()
{
    return s_ApiKey;
}
