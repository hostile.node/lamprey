//------------------------------------------------------------------------------
// This file is part of Lamprey.
//
// Lamprey is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Lamprey is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Lamprey. If not, see <https://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#include <atomic>
#include <chrono>
#include <iostream>
#include <fstream>
#include <memory>
#include <string>
#include <vector>
#include <thread>

#include <inttypes.h>

#include "googlesearch.h"
#include "log.h"
#include "utils.h"
#include "querycache.h"
#include "settings.h"
#include "version.h"

using SearchTermCollection = std::vector<std::string>;

static QueryCache s_QueryCache;
GoogleSearch* g_pGoogleSearch = nullptr;
std::ofstream g_SummaryFile;
std::ofstream g_ResultsFile;

std::atomic_uint64_t g_TotalQueries;
std::atomic_uint64_t g_CompletedQueries;

class SearchState
{
public:
	void Add(const SearchTermCollection& collection);
	size_t GetCollectionCount() const;
	size_t GetSearchTermsCount(size_t collectionIndex) const;
	const std::string& GetSearchTerm(size_t collectionIndex, size_t searchTermIndex) const;

private:
	std::vector<SearchTermCollection> m_Collections;
	std::vector<size_t> m_Progress;
};

void SearchState::Add(const SearchTermCollection& collection)
{
	m_Collections.push_back(collection);
	m_Progress.push_back(0);
}

size_t SearchState::GetCollectionCount() const
{
	return m_Collections.size();
}

size_t SearchState::GetSearchTermsCount(size_t collectionIndex) const
{
	return m_Collections[collectionIndex].size();
}

const std::string& SearchState::GetSearchTerm(size_t collectionIndex, size_t searchTermIndex) const
{
	return m_Collections[collectionIndex][searchTermIndex];
}

static bool LoadSearchTerms(const std::string& filename, SearchTermCollection& results)
{
	if (Utils::ReadLines(filename, results))
	{
		return true;
	}
	else
	{
		Log::Error("Couldn't open file '%s'.", filename.c_str());
		return false;
	}
}

static void SearchCallback(const QueryData& queryData, bool success, uint64_t results)
{
	if (success == false)
	{
		Log::Error("Failed to search for %s", queryData.query.Get().c_str());
		return;
	}

	s_QueryCache.Add(queryData.query.Get());
	g_CompletedQueries++;

	float total = static_cast<float>(g_CompletedQueries.load()) / static_cast<float>(g_TotalQueries.load()) * 100.0f;
	Log::Info("[%.1f%%] %s: %" PRIu64 " results.", total, queryData.query.Get().c_str(), results);

	g_SummaryFile << results << "," << queryData.query.Get() << "\n";
	g_SummaryFile.flush();

	if (results > 0)
	{		
		for (QueryResult result : queryData.results)
		{
			g_ResultsFile << queryData.query.Get() << "," << result.GetTitle() << "," << result.GetUrl() << "\n";
		}

		g_ResultsFile.flush();
	}
}

static void RecursiveSearch(SearchState& searchState, size_t collectionIndex, size_t searchTermIndex, const std::string& query)
{
	if (collectionIndex >= searchState.GetCollectionCount())
	{
		if (s_QueryCache.Contains(query))
		{
			g_CompletedQueries++;
		}
		else
		{
			g_pGoogleSearch->AsyncQuery(query);
		}
	}
	else
	{
		size_t searchTermsCount = searchState.GetSearchTermsCount(collectionIndex);
		for (size_t i = 0; i < searchTermsCount; ++i)
		{
			std::string newQuery = query + " +\"" + searchState.GetSearchTerm(collectionIndex, i) + "\"";
			RecursiveSearch(searchState, collectionIndex + 1, 0, newQuery);
		}
	}
}

static void BeginSearch(SearchState& searchState)
{
	size_t searchTermsCount = searchState.GetSearchTermsCount(0);
	for (size_t i = 0; i < searchTermsCount; ++i)
	{
		RecursiveSearch(searchState, 1, 0, "\"" + searchState.GetSearchTerm(0, i) + "\"");
	}
}

static uint64_t CalculateTotalQueries(const SearchState& searchState)
{
	uint64_t total = 0u;
	int collectionCount = searchState.GetCollectionCount();
	if (collectionCount > 0)
	{
		total = searchState.GetSearchTermsCount(0);
	}

	for (int i = 1; i < collectionCount; ++i)
	{
		total *= searchState.GetSearchTermsCount(i);
	}

	return total;
}

int main(int argc, char** argv)
{
	std::shared_ptr<TTYLogger> ttyLogger = std::make_shared<TTYLogger>();
	Log::AddLogTarget(ttyLogger);
	g_pGoogleSearch = new GoogleSearch(&SearchCallback);

	Log::Info("===== Lamprey %d.%d.%d =====", s_LampreyVersionMajor, s_LampreyVersionMinor, s_LampreyVersionBuild);

	if (argc <= 1)
	{
		Log::Info("Usage: lamprey collection1 collection2 [collectionN]");
		return 0;
	}

	Settings::Load();
	s_QueryCache.Populate("summary.csv");

	g_SummaryFile.open("summary.csv", std::ios::out | std::ios::app);
	g_ResultsFile.open("results.csv", std::ios::out | std::ios::app);

	SearchState searchState;
	for (int argi = 1; argi < argc; ++argi)
	{
		std::string filename = argv[argi];
		SearchTermCollection collection;
		if (LoadSearchTerms(filename, collection))
		{
			Log::Info("Loaded %d search terms from '%s'.", collection.size(), filename.c_str());
		}
		else
		{
			Log::Error("Failed to load search terms for collection '%s'.", argv[argi]);
			return -1;
		}
		searchState.Add(collection);
	}

	g_TotalQueries = CalculateTotalQueries(searchState);

	bool performSearches = true;
	if (s_QueryCache.Size() > 0u)
	{
		Log::Info("%d previous results have been found. Do you wish to resume? y/n", static_cast<int>(s_QueryCache.Size()));
		std::string s;
		std::cin >> s;
		if (s.length() != 1 || s[0] != 'y')
		{
			performSearches = false;
		}
	}

	if (performSearches)
	{
		int queriesToPerform = static_cast<int>(g_TotalQueries.load()) - static_cast<int>(s_QueryCache.Size());
		const int queriesPerDay = 10000;
		const float costPerThousand = 5.00f;
		float cost = static_cast<float>(queriesToPerform) / 1000.0f * costPerThousand;
		if (queriesToPerform > queriesPerDay)
		{
			Log::Info("This run will perform %d searches. As the cap is %d per day, you'll not be able to complete this in a single run.", queriesToPerform, queriesPerDay);
		}
		else
		{
			Log::Info("This run will perform %d searches.", queriesToPerform);
		}
		Log::Info("The estimated cost for the run is US$ %.2f.", cost);
		Log::Info("Do you wish to continue? y/n");

		std::string s;
		std::cin >> s;
		if (s.length() != 1 || s[0] != 'y')
		{
			performSearches = false;
		}
	}

	if (performSearches)
	{
		BeginSearch(searchState);

		while(g_pGoogleSearch->HasFinished() == false)
		{
			std::this_thread::sleep_for(std::chrono::seconds(1));
		}
	}

	delete g_pGoogleSearch;
	g_ResultsFile.close();
	g_SummaryFile.close();
	Log::RemoveLogTarget(ttyLogger);
	return 0;
}
