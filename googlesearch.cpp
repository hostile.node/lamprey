//------------------------------------------------------------------------------
// This file is part of Lamprey.
//
// Lamprey is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Lamprey is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Lamprey. If not, see <https://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#include "googlesearch.h"

#include <iostream>
#include <fstream>

#include <curl/curl.h>

#include <stdio.h>

#include "log.h"
#include "settings.h"

// This needs to be a static function as libcurl is a C library and will segfault if passed
// a local lambda.
static size_t WriteMemoryCallback(void* pContents, size_t size, size_t nmemb, void* pUserData)
{
	size_t realSize = size * nmemb;
	std::string& data = *reinterpret_cast<std::string*>(pUserData);
	size_t curDataSize = data.size();
	data.resize(curDataSize + realSize);
	memcpy(&data[curDataSize], pContents, realSize);
	return realSize;
}

GoogleSearch::GoogleSearch(QueryCallback pCallback):
m_pQueryCallback(pCallback),
m_QueryThreadStopFlag(false)
{
	m_pCurlHandle = curl_easy_init();
	m_QueryThread = std::thread(GoogleSearch::ThreadMain, this);
}

GoogleSearch::~GoogleSearch()
{
	if (m_QueryThread.joinable())
	{
		m_QueryThreadStopFlag = true;
		m_QueryThread.join();
	}

	curl_easy_cleanup(m_pCurlHandle);
}

void GoogleSearch::AsyncQuery(const std::string& query)
{
	std::lock_guard<std::mutex> lock(m_QueryQueueMutex);
	QueryData queryData;
	queryData.query = query;
	m_QueryQueue.push_back(queryData);
}

bool GoogleSearch::ConsumeQueue(QueryData& queryData)
{
	std::lock_guard<std::mutex> lock(m_QueryQueueMutex);
	if (m_QueryQueue.empty())
	{
		return false;
	}
	else
	{
		queryData = m_QueryQueue.front();
		m_QueryQueue.pop_front();
		return true;
	}
}

void GoogleSearch::ThreadMain(GoogleSearch* pGoogleSearch)
{
	while (pGoogleSearch->m_QueryThreadStopFlag == false)
	{
		pGoogleSearch->m_InFlight = true;
		QueryData queryData;
		if (pGoogleSearch->ConsumeQueue(queryData) == false)
		{
			pGoogleSearch->m_InFlight = false;
			continue;
		}

		std::stringstream url;
		url << "https://www.googleapis.com/customsearch/v1?q=" << queryData.query.GetEncoded() << "&cx=" << Settings::GetSearchEngineId() << "&key=" << Settings::GetApiKey();

		if (queryData.state.IsValid())
		{
			url << "&start=" << queryData.state.GetCurrentStart();
		}

		CURL* pCurlHandle = pGoogleSearch->m_pCurlHandle;
		const int numRetries = 5;
		for (int retry = 0; retry < numRetries; ++retry)
		{
			bool success = false;
			char pErrorBuffer[CURL_ERROR_SIZE];
			curl_easy_setopt(pCurlHandle, CURLOPT_ERRORBUFFER, pErrorBuffer);
			curl_easy_setopt(pCurlHandle, CURLOPT_URL, url.str().c_str());
			curl_easy_setopt(pCurlHandle, CURLOPT_WRITEFUNCTION, WriteMemoryCallback);
			curl_easy_setopt(pCurlHandle, CURLOPT_WRITEDATA, &pGoogleSearch->m_CurlData);
			curl_easy_setopt(pCurlHandle, CURLOPT_USERAGENT, "libcurl-agent/1.0");
			curl_easy_setopt(pCurlHandle, CURLOPT_TIMEOUT, 10L);

			CURLcode result = curl_easy_perform(pCurlHandle);
			if (result != CURLE_OK)
			{
				if (retry == numRetries - 1)
				{
					pGoogleSearch->m_pQueryCallback(queryData, false, 0);
				}
				else
				{
					Log::Warning("curl error, retrying...");
				}
			}
			else
			{
				std::string curlData = pGoogleSearch->m_CurlData;
				std::string filteredCurlData = FilterCurlData(curlData);
				json data = json::parse(filteredCurlData, nullptr, false);

				json::iterator errorIt = data.find("error");
				if (errorIt != data.end())
				{
					json error = data["error"];
					std::string errorMessage = "Google search error.";
					json::iterator messageIt = error.find("message");
					if (messageIt != error.end())
					{
						json message = error["message"];
						if (message.is_string())
						{
							errorMessage = message.get<std::string>();
						}
					}

					if (retry == numRetries - 1)
					{
						Log::Error("%s", errorMessage.c_str());
					}
					else
					{
						Log::Warning("%s Retrying...", errorMessage.c_str());
					}
				}
				else
				{
					uint64_t totalResults = 0;
					if (ExtractTotalResults(data, totalResults))
					{
						queryData.state.SetResultCount(totalResults);
					}

					if (totalResults > 0)
					{
						ExtractResults(data, queryData.results);
					}

					pGoogleSearch->m_pQueryCallback(queryData, true, totalResults);
					success = true;
				}
			}

			// The buffer containing the data from the request needs to be cleared after being used,
			// so further requests have a clean slate.
			pGoogleSearch->m_CurlData.clear();

			if (success)
			{
				break;
			}
			else
			{
				std::this_thread::sleep_for(std::chrono::seconds(3));
			}
		}
		
		pGoogleSearch->m_InFlight = false;
	}
}

bool GoogleSearch::HasFinished() const
{
	return m_QueryQueue.empty() && m_InFlight == false;
}

std::string GoogleSearch::FilterCurlData(const std::string& data)
{
	size_t dataSize = data.size();
	size_t filteredDataSize = 0u;
	std::string filteredData;
	filteredData.resize(dataSize);
	for (size_t i = 0u; i < dataSize; i++)
	{
		if (data[i] >= 32 && data[i] <= 126)
		{
			filteredData[filteredDataSize++] = data[i];
		}
	}
	filteredData[filteredDataSize] = '\0';
	return filteredData;
}

bool GoogleSearch::ExtractTotalResults(const json& data, uint64_t& result)
{
	if (data.find("queries") == data.cend()) { return false; }
	const json& queries = data["queries"];
	if (queries.is_null()) return false;

	if (queries.find("request") == queries.cend()) { return false; }
	const json& request = queries["request"];
	if (!request.is_array()) return false;

	for (auto& entry : request)
	{
		if (entry.find("totalResults") == entry.cend())
		{
			return false;
		}

		// Oddly enough, the "totalResults" entry returned by the API isn't an integer, but a string.
		const json& totalResults = entry["totalResults"];
		if (totalResults.is_string())
		{
			char* pEnd;
			result = strtoull(totalResults.get<std::string>().c_str(), &pEnd, 10);
			return true;
		}
	}
	return false;
}

bool GoogleSearch::ExtractResults(const json& data, QueryResults& results)
{
	if (data.find("items") == data.cend())
	{
		return false;
	}

	const json& items = data["items"];
	if (!items.is_array()) return false;

	for (auto& item : items)
	{
		std::string url;
		json::const_iterator it;
		if ((it = item.find("link")) != item.end())
		{
			url = it->get<std::string>();
		}

		std::string title;
		if ((it = item.find("title")) != item.end())
		{
			title = it->get<std::string>();
		}

		results.emplace_back(url, title);
	}
	return true;
}
